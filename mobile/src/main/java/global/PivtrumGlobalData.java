package global;

import java.util.ArrayList;
import java.util.List;

import pivtrum.PivtrumPeerData;

/**
 * Created by furszy on 7/2/17.
 */

public class PivtrumGlobalData {

    public static final String FURSZY_TESTNET_SERVER = "104.236.80.135";

    //    public static final String[] TRUSTED_NODES = new String[]{"explorer.bitcoinreal.org","node-1.bitcoinreal.org", "node-2.bitcoinreal.org"};
    public static final String[] TRUSTED_NODES = new String[]{"iquidus.rainbit.me",
            "bulwark.rainbit.me",
            "seed1.rainibit.me",
            "seed2.rainbit.me",
            "seed3.rainibit.me",
            "seed4.rainbit.me",
            "seed5.rainibit.me",
            "seed6.rainbit.me",
            "seed7.rainibit.me",
            "seed8.rainbit.me",
            "seed9.rainibit.me",
            "seed10.rainbit.me",
            "seed11.rainibit.me",
            "seed12.rainbit.me",
            "seed13.rainibit.me",
            "seed14.rainbit.me",
            "seed15.rainibit.me",
            "seed16.rainbit.me",
            "seed17.rainibit.me",
            "seed18.rainbit.me",
            "seed19.rainibit.me",
            "seed20.rainbit.me",
            "seed21.rainibit.me",
            "seed22.rainbit.me",
            "seed23.rainibit.me",
            "seed24.rainbit.me",
            "seed25.rainibit.me",
            "seed26.rainbit.me",
            "seed27.rainibit.me",
            "seed28.rainbit.me",
            "seed29.rainibit.me",
            "seed30.rainbit.me",
            "seed31.rainibit.me",
            "seed32.rainbit.me",
            "seed33.rainibit.me",
            "seed34.rainibit.me",
            "seed35.rainibit.me"};

    public static final List<PivtrumPeerData> listTrustedHosts(){
        List<PivtrumPeerData> list = new ArrayList<>();
        list.add(new PivtrumPeerData(FURSZY_TESTNET_SERVER,8443,55552));
        for (String trustedNode : TRUSTED_NODES) {
//            list.add(new PivtrumPeerData(trustedNode,13319,55552));
            list.add(new PivtrumPeerData(trustedNode,2919,55552));
        }
        return list;
    }
}